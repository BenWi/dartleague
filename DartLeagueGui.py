import sys
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QFileDialog
from AddGameDialog import AddGameDialog
from os.path import expanduser

from DartLeague_DatabaseConnector import *

# todo Points per game
# todo calculate points


class PlayerStatsTableModel(QtCore.QAbstractTableModel):
    def __init__(self, wins, lasts, played_games, humiliation_games, parent=None, *args):
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.wins = wins
        self.lasts = lasts
        self.played_games = played_games
        self.humiliation_games = humiliation_games

    def rowCount(self, parent):
        return max(len(self.wins.keys()), len(self.lasts.keys()))

    def columnCount(self, parent):
        return 6

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()
        elif role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()
        if index.column() == 0:
            # wins
            return QtCore.QVariant(self.wins[GameType(index.row())][0])
        elif index.column() == 1:
            # lasts
            return QtCore.QVariant(self.lasts[GameType(index.row())][0])
        elif index.column() == 2:
            # played games
            if self.played_games[GameType(index.row())]:
                return QtCore.QVariant(len(self.played_games[GameType(index.row())]))
            else:
                return QtCore.QVariant(0)
        elif index.column() == 3:
            # win rate
            if self.wins[GameType(index.row())] and self.played_games[GameType(index.row())]:
                return QtCore.QVariant(100.0 * self.wins[GameType(index.row())][0]
                                       / len(self.played_games[GameType(index.row())]))
            else:
                return QtCore.QVariant(0)
        elif index.column() == 4:
            # loose rate
            if self.lasts[GameType(index.row())] and self.played_games[GameType(index.row())]:
                return QtCore.QVariant(100.0 * self.lasts[GameType(index.row())][0]
                                       / len(self.played_games[GameType(index.row())]))
            else:
                return QtCore.QVariant(0)
        elif index.column() == 5:
            # humiliations
            if self.humiliation_games[GameType(index.row())]:
                return QtCore.QVariant(len(self.humiliation_games[GameType(index.row())]))
            else:
                return QtCore.QVariant(0)

    def headerData(self, column, orientation, role=QtCore.Qt.DisplayRole):
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()
        if orientation == QtCore.Qt.Horizontal:
            if column == 0:
                return QtCore.QVariant('First')
            elif column == 1:
                return QtCore.QVariant('Last')
            elif column == 2:
                return QtCore.QVariant('Played')
            elif column == 3:
                return QtCore.QVariant('W. Rate')
            elif column == 4:
                return QtCore.QVariant('L. Rate')
            elif column == 5:
                return QtCore.QVariant('0pts')
        elif orientation == QtCore.Qt.Vertical:
            return QtCore.QVariant(GameType.get_string(GameType(column)))

qtCreatorFile = "AddPlayerDialog.ui"  # Enter file here.
Ui_AddPlayerDialog, QtBaseClass = uic.loadUiType(qtCreatorFile)
class AddPlayerDialog(QtGui.QDialog, Ui_AddPlayerDialog):
    def __init__(self, parent=None):
        QtGui.QDialog .__init__(self, parent)
        self.setupUi(self)


qtCreatorFile = "DartLeagueGui.ui"  # Enter file here.
Ui_DartLeague, QtBaseClass = uic.loadUiType(qtCreatorFile)
class DartLeagueGui(QtGui.QMainWindow, Ui_DartLeague):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_DartLeague.__init__(self)
        self.setupUi(self)
        self.connect_signals()

        self.database_lineEdit.setText(os.path.join(expanduser("~"), "DartLeague.db"))
        self.dartleagueDB = None
        self.player_alias_id_dict = dict()

        self.num_recent_games = 10

        self.reload_all()
        self.season_spin_box.setValue(self.dartleagueDB.get_highest_season())

    def connect_signals(self):
        print('connect')

        # connect file select button
        self.change_database_button.clicked.connect(self.change_database_button_clicked)
        # connect add player button
        self.add_player_button.clicked.connect(self.add_player_button_clicked)
        # connect remove player button
        self.remove_player_button.clicked.connect(self.remove_player_button_clicked)
        # connect add game button
        self.add_game_button.clicked.connect(self.add_game_button_clicked)
        # connect remove game button
        self.remove_game_button.clicked.connect(self.remove_game_button_clicked)
        # connect new season button
        self.new_season_button.clicked.connect(self.new_season_button_clicked)
        # connect player list change
        self.players_listWidget.currentItemChanged.connect(self.update_players_stats_table)
        # connect recent game change
        self.recent_games_listWidget.currentItemChanged.connect(self.update_recent_games_results_list)
        # connect filter statistics checkbox
        self.filter_statistics_checkbox.clicked.connect(self.reload_all)
        # connect season spin box
        self.season_spin_box.valueChanged.connect(self.reload_all)

    def change_database_button_clicked(self):
        cwd = os.path.dirname(str(self.database_lineEdit.text()))
        if not cwd or not os.path.exists(cwd):
            cwd = ''

        tfile = QFileDialog.getOpenFileName(self, "Select database to open", cwd, "Database (*.db)")
        if tfile:
            self.database_lineEdit.setText(tfile)
            self.reload_all()

    def add_player_button_clicked(self):
        print ("add player")
        qd = AddPlayerDialog()
        res = qd.exec_()
        if res:
            name = str(qd.playername_lineEdit.text())
            alias = str(qd.playeralias_lineEdit.text())
            if name and alias:
                self.dartleagueDB.add_player(name, alias)
                self.update_players_list()

    def remove_player_button_clicked(self):
        curr_item = self.players_listWidget.currentItem()
        if curr_item:
            player_id = self.player_alias_id_dict[str(curr_item.text())]
            ret = QtGui.QMessageBox.question(self, 'Delete Player?',
                                       'Do you really want to delete the player ' + curr_item.text() + ' (ID: ' +
                                       str(player_id) + ')',
                                       QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
            if ret == QtGui.QMessageBox.Ok:
                self.dartleagueDB.safely_remove_player(player_id)
                self.reload_all()
        
    def add_game_button_clicked(self):
        print ("add game")
        game = AddGameDialog.get_game_values(
            self.dartleagueDB.get_players(),
            self.season_spin_box.value(),
            self.season_spin_box.maximum())
        if game:
            game_id = self.dartleagueDB.add_game(game[1], game[0], game[2], len(game[3]), game[4])
            for rank, player in enumerate(game[3]):
                self.dartleagueDB.add_player_game_relation(self.player_alias_id_dict[player]
                                                           , game_id, rank+1)
            self.reload_all()

    def remove_game_button_clicked(self):
        curr_index = self.recent_games_listWidget.currentRow()
        if curr_index >= 0:
            ids = self.dartleagueDB.get_recent_game_ids(self.num_recent_games)
            current_game_id = ids[curr_index]
            ret = QtGui.QMessageBox.question(self, 'Delete Game?',
                                             'Do you really want to delete the game with ID  ' + str(current_game_id),
                                             QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
            if ret == QtGui.QMessageBox.Ok:
                self.dartleagueDB.remove_game(current_game_id)
                self.reload_all()

    def reload_all(self):
        self.dartleagueDB = DartLeague_DatabaseConnector(str(self.database_lineEdit.text()))
        self.update_players_list()
        self.update_game_types_list()
        self.update_recent_games_list()

        self.update_players_stats_table()
        self.update_game_type_stats_table()
        self.update_recent_games_results_list()

        self.season_spin_box.setMaximum(self.dartleagueDB.get_highest_season())

    def update_players_list(self):
        curr_item = self.players_listWidget.currentRow()
        self.players_listWidget.clear()
        players = self.dartleagueDB.get_players()
        if players:
            for player in players:
                item = QtGui.QListWidgetItem()
                item.setText(str(player[2]))
                self.player_alias_id_dict[str(player[2])] = player[0]
                self.players_listWidget.addItem(item)

            if curr_item >= 0:
                self.players_listWidget.setCurrentRow(curr_item)
            else:
                self.players_listWidget.setCurrentRow(0)
                self.players_listWidget.item(0).setSelected(True)

    def update_players_stats_table(self):
        curr_item = self.players_listWidget.currentItem()
        if not curr_item:
            sel_item = self.players_listWidget.selectedItems()
            if sel_item:
                self.players_listWidget.setCurrentItem(sel_item[0])
        if curr_item:
            curr_item = curr_item
            if self.filter_statistics_checkbox.isChecked():
                current_season = self.season_spin_box.value()
            else:
                current_season = []
            player_id = self.player_alias_id_dict[str(curr_item.text())]
            wins = dict()
            lasts = dict()
            played_games = dict()
            humiliation_games = dict()
            for game in GameType:
                wins[game] = self.dartleagueDB.stats_count_placing_for_gametype(game, 1, player_id, current_season)
                lasts[game] = self.dartleagueDB.stats_count_last_place_for_gametype(game, player_id, current_season)
                played_games[game] = self.dartleagueDB.get_played_games_of_type_for_player(
                    game, player_id, current_season)
                humiliation_games[game] = self.dartleagueDB.get_humiliation_games_for_player(
                    player_id, game, current_season)

            tablemodel = PlayerStatsTableModel(wins, lasts, played_games, humiliation_games, self)
            self.player_stats_tableView.setModel(tablemodel)
            self.player_stats_tableView.resizeColumnsToContents()

    def update_game_types_list(self):
        self.game_types_listWidget.clear()
        for game in GameType:
            item = QtGui.QListWidgetItem()
            item.setText(GameType.get_string(game))
            self.game_types_listWidget.addItem(item)
        self.game_types_listWidget.item(0).setSelected(True)

    def update_game_type_stats_table(self):
        print('update game type stats table not implemented')

    def update_recent_games_list(self):
        self.recent_games_listWidget.clear()
        games = self.dartleagueDB.get_recent_games(self.num_recent_games)
        if games:
            for game in games:
                item = QtGui.QListWidgetItem()
                item.setText(str(game[2]) + '\t' + GameType.get_string(GameType(game[1])) + '\tID:' + str(game[0]))
                self.recent_games_listWidget.addItem(item)

    def update_recent_games_results_list(self):
        self.recent_game_results_listWidget.clear()
        curr_index = self.recent_games_listWidget.currentRow()
        if curr_index >= 0:
            ids = self.dartleagueDB.get_recent_game_ids(self.num_recent_games)
            current_game_id = ids[curr_index]
            # get the players for current game_id and list them
            player_placings = self.dartleagueDB.get_players_for_game_id(current_game_id)
            humiliation_ids = self.dartleagueDB.get_humiliation_games()
            for player_id in player_placings:
                if player_id not in self.player_alias_id_dict.values():
                    player_alias = self.dartleagueDB.get_player_alias_for_id(player_id)
                else:
                    player_alias = self.player_alias_id_dict.keys()[self.player_alias_id_dict.values().index(player_id)]
                item = QtGui.QListWidgetItem()
                item.setText(str(player_alias))
                if humiliation_ids and player_id == player_placings[-1] and current_game_id in humiliation_ids:
                    item.setTextColor(QtGui.QColor(255, 0, 255))
                self.recent_game_results_listWidget.addItem(item)

    def new_season_button_clicked(self):
        self.season_spin_box.setMaximum(self.dartleagueDB.get_highest_season()+1)
        self.season_spin_box.setValue(self.dartleagueDB.get_highest_season()+1)


def create_test_database(path):
    db = DartLeague_DatabaseConnector(path)
    # players
    l = db.add_player('Lukas Caup', 'Luki')
    r = db.add_player('Roman Kloepfer', 'Romantic')
    b = db.add_player('Benjamin Wilking', 'Benni')
    ab = db.add_player('Abraham', 'AB')
    f = db.add_player('Fabian Stumpf', 'Fabi')

    #season 1
    gid = db.add_game(GameType.CRICKET, '2016.11.25', 1, 3, False)
    db.add_player_game_relation(l, gid, 1)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.CRICKET, '2016.11.26', 1, 3, False)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.CRICKET, '2016.11.26', 1, 3, True)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 1)

    gid = db.add_game(GameType.CRICKET, '2016.11.27', 1, 5, True)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 3)
    db.add_player_game_relation(f, gid, 4)
    db.add_player_game_relation(ab, gid, 5)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2016.11.25', 1, 3, False)
    db.add_player_game_relation(l, gid, 1)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2016.11.26', 1, 3, False)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2016.11.26', 1, 3, True)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 2)

    gid = db.add_game(GameType.ALL_FIVES, '2016.11.27', 1, 5, True)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 3)
    db.add_player_game_relation(f, gid, 4)
    db.add_player_game_relation(ab, gid, 5)

    # season 2
    gid = db.add_game(GameType.CRICKET, '2017.11.25', 2, 3, False)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 1)

    gid = db.add_game(GameType.CRICKET, '2017.11.26', 2, 3, False)
    db.add_player_game_relation(l, gid, 1)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.CRICKET, '2017.11.26', 2, 3, True)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 3)
    db.add_player_game_relation(b, gid, 1)

    gid = db.add_game(GameType.CRICKET, '2017.11.27', 2, 5, True)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 1)
    db.add_player_game_relation(f, gid, 5)
    db.add_player_game_relation(ab, gid, 4)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2017.11.25', 2, 3, False)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 2)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2017.11.26', 2, 3, False)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 1)
    db.add_player_game_relation(b, gid, 3)

    gid = db.add_game(GameType.THREE_ZERO_ONE, '2017.11.26', 2, 3, True)
    db.add_player_game_relation(l, gid, 3)
    db.add_player_game_relation(r, gid, 2)
    db.add_player_game_relation(b, gid, 1)

    gid = db.add_game(GameType.ALL_FIVES, '2017.11.27', 2, 5, True)
    db.add_player_game_relation(l, gid, 2)
    db.add_player_game_relation(r, gid, 3)
    db.add_player_game_relation(b, gid, 1)
    db.add_player_game_relation(f, gid, 5)
    db.add_player_game_relation(ab, gid, 4)

if __name__ == "__main__":

    path = r'C:\Users\Benni\DartLeague.db'
    if not os.path.exists(path):
        create_test_database(path)

    app = QtGui.QApplication(sys.argv)
    window = DartLeagueGui()
    window.show()
    sys.exit(app.exec_())