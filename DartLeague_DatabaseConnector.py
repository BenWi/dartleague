import sqlite3
import os
from GameType import GameType


class DartLeague_DatabaseConnector:
    def __init__(self, path):
        if os.path.exists(path):
            have_database = True
        else:
            have_database = False
        self.connection = sqlite3.connect(path)
        self.cursor = self.connection.cursor()

        if not have_database:
            self.init_database()

    def __del__(self):
        self.connection.close()

    def init_database(self):

        # Create table
        self.cursor.execute('''CREATE TABLE `player` (
                        `player_id`	INTEGER,
                        `name`	TEXT,
                        `alias`	TEXT,
                        'disabled' BOOL
                         )''')

        self.cursor.execute('''CREATE TABLE `game` (
                        `game_id`	INTEGER,
                        `type`	INTEGER,
                        `date`	TEXT,
                        `season` INTEGER,
                        'num_players' INTEGER,
                        'humiliation' BOOL
                        )''')

        self.cursor.execute('''CREATE TABLE `player_game` (
                        `player_id`	INTEGER,
                        `game_id`	INTEGER,
                        `placing`	INTEGER
                        );''')

        # Save (commit) the changes
        self.connection.commit()

    def add_player(self, name, alias):
        # Insert a new player
        player_id = self.get_new_player_id()
        new_id = (player_id, name, alias)
        self.cursor.execute("INSERT INTO player VALUES (?, ?, ?, 0)", new_id)

        # Save (commit) the changes
        self.connection.commit()
        return player_id

    def safely_remove_player(self, player_id):
        # check if the player is used in any game
        played_games = self.get_played_games_for_player(player_id)
        if played_games:
            print('Can not delete player with ID ' + str(player_id) + '. Player is part of games ' + str(played_games))
            print('Going to disable the player for new games')
            self.disable_player(player_id)
        else:
            self.delete_player(player_id)

    def enable_player(self, player_id):
        self.cursor.execute("UPDATE player SET disabled = 0 WHERE player_id = ?", (player_id,))
        # Save (commit) the changes
        self.connection.commit()

    def disable_player(self, player_id):
        self.cursor.execute("UPDATE player SET disabled = 1 WHERE player_id = ?", (player_id,))
        # Save (commit) the changes
        self.connection.commit()

    def delete_player(self, player_id):
        self.cursor.execute("DELETE FROM player WHERE player_id = ?", (player_id,))
        # Save (commit) the changes
        self.connection.commit()

    def get_player_alias_for_id(self, player_id):
        alias = self.cursor.execute("SELECT alias FROM player WHERE player_id = ?", (player_id,))
        alias = str(alias.fetchone()[0])
        if alias:
            return alias
        else:
            return None

    def get_new_player_id(self):
        highest = self.cursor.execute("SELECT player_id from player_game ORDER BY player_id DESC LIMIT 1")
        highest_id = highest.fetchone()
        if highest_id:
            return highest_id[0]+1
        else:
            highest = self.cursor.execute("SELECT player_id from player ORDER BY player_id DESC LIMIT 1")
            highest_id = highest.fetchone()
            if highest_id:
                return highest_id[0] + 1
            else:
                return 0

    def get_players(self):
        players = self.cursor.execute("SELECT * from player  WHERE disabled = 0 ORDER BY player_id ASC")
        players = players.fetchall()
        if players:
            return players
        else:
            return None

    def add_game(self, gametype, date, season, num_players, humiliation):
        # Insert a new player
        game_id = self.get_new_game_id()
        if not isinstance(gametype, GameType):
            return
        new_id = (game_id, gametype.value, date, season, num_players, humiliation)
        self.cursor.execute("INSERT INTO game VALUES (?, ?, ?, ?, ?, ?)", new_id)

        # Save (commit) the changes
        self.connection.commit()
        return game_id

    def remove_game(self, game_id):
        # remove game itself
        self.cursor.execute("DELETE FROM game WHERE game_id = ?", (game_id,))
        # remove player_game relations
        self.cursor.execute("DELETE FROM player_game WHERE game_id = ?", (game_id,))
        # Save (commit) the changes
        self.connection.commit()

    def get_new_game_id(self):
        highest = self.cursor.execute("SELECT game_id from player_game ORDER BY game_id DESC LIMIT 1")
        highest_id = highest.fetchone()
        if highest_id:
            return highest_id[0]+1
        else:
            return 0

    def get_recent_games(self, num_of_games):
        games = self.cursor.execute("SELECT * from game ORDER BY date DESC, game_id LIMIT ?", (num_of_games,))
        games = games.fetchall()
        if games:
            return games
        else:
            return None

    def get_recent_game_ids(self, num_of_games):
        ids = self.cursor.execute("SELECT game_id from game ORDER BY date DESC, game_id LIMIT ?", (num_of_games,))
        ids = ids.fetchall()
        if ids:
            return [n[0] for n in ids]
        else:
            return None

    def add_player_game_relation(self, player_id, game_id, placing):
        # Insert a new relation
        new_id = (player_id, game_id, placing)
        self.cursor.execute("INSERT INTO player_game VALUES (?, ?, ?)", new_id)

        # Save (commit) the changes
        self.connection.commit()

    def get_highest_season(self):
        season = self.cursor.execute("SELECT season from game ORDER BY season DESC LIMIT 1")
        season = season.fetchone()
        if season:
            return season[0]
        else:
            return 1

    def get_gametype_from_game_id(self, game_id):
        gametype = self.cursor.execute("SELECT type FROM game WHERE game_id=?", (game_id,))
        if gametype:
            gametype = gametype.fetchone()
            return GameType(gametype[0])
        else:
            return None

    def get_players_for_game_id(self, game_id):
        ids = self.cursor.execute("SELECT player_id from player_game WHERE game_id = ? ORDER BY placing ASC", (game_id,))
        ids = ids.fetchall()
        if ids:
            return [n[0] for n in ids]
        else:
            return None

    def get_humiliation_games(self, gametype=[], season=[]):
        if gametype and season:
            ids = self.cursor.execute("SELECT game_id from game WHERE humiliation = 1 and type = ? "
                                      "and season = ?", (gametype.value, season))
        elif gametype:
            ids = self.cursor.execute("SELECT game_id from game WHERE humiliation = 1 and type = ?", (gametype.value,))
        elif season:
            ids = self.cursor.execute("SELECT game_id from game WHERE humiliation = 1 and season = ?", (season,))
        else:
            ids = self.cursor.execute("SELECT game_id from game WHERE humiliation = 1")
        if ids:
            ids = ids.fetchall()
            return [n[0] for n in ids]
        else:
            return None

    def stats_count_placing(self, placing, player_id, season=[]):
        game_ids = self.cursor.execute("SELECT game_id FROM player_game WHERE placing=? and player_id=?",
                                       (placing, player_id))
        if game_ids:
            game_ids = game_ids.fetchall()

        games_ids_for_season = []
        if season:
            games_ids_for_season = self.cursor.execute("SELECT game_id FROM game WHERE season = ?", (season,))
            if games_ids_for_season:
                games_ids_for_season = games_ids_for_season.fetchall()
                games_ids_for_season = [n[0] for n in games_ids_for_season]

        if game_ids:
            if season:
                game_ids = [n[0] for n in game_ids if n[0] in games_ids_for_season]
            else:
                game_ids = [n[0] for n in game_ids]
            return len(game_ids), game_ids
        else:
            return None, None

    def stats_count_last_place(self, player_id, season = []):
        games_placing = self.cursor.execute("SELECT game_id, placing from player_game WHERE player_id = ?", (player_id,))
        games_placing = games_placing.fetchall()
        game_ids = [n[0] for n in games_placing]
        placings = [n[1] for n in games_placing]
        count = 0
        lost_games = []
        for gid, placing in zip(game_ids, placings):
            num_players = self.cursor.execute("SELECT num_players from game WHERE game_id = ?", (gid,))
            num_players = num_players.fetchone()[0]
            if num_players == placing:
                count += 1
                lost_games.append(gid)
        return count, lost_games

    def stats_count_placing_for_gametype(self, gametype, placing, player_id, season=[]):
        if season:
            played_games_in_season = self.get_played_games_of_type_for_player(gametype, player_id, season)
        total, game_ids = self.stats_count_placing(placing, player_id)
        count = 0
        won_ids = []
        if game_ids:
            for gid in game_ids:
                if season and played_games_in_season and gid not in played_games_in_season:
                    continue
                gtype = self.get_gametype_from_game_id(gid)
                if gtype == gametype:
                    count += 1
                    won_ids.append(gid)
        return count, won_ids

    def stats_count_last_place_for_gametype(self, gametype, player_id, season=[]):
        if season:
            played_games_in_season = self.get_played_games_of_type_for_player(gametype, player_id, season)
        count, gids = self.stats_count_last_place(player_id)
        count = 0
        lost_ids = []
        if gids:
            for gid in gids:
                if season and played_games_in_season and gid not in played_games_in_season:
                    continue
                gtype = self.get_gametype_from_game_id(gid)
                if gtype == gametype:
                    count += 1
                    lost_ids.append(gid)
        return count, lost_ids

    def get_games_of_type(self, gametype, season=[]):
        if season:
            game_ids = self.cursor.execute("SELECT game_id from game WHERE season = ? and  type = ?",
                                           (season, gametype.value))
        else:
            game_ids = self.cursor.execute("SELECT game_id from game WHERE type = ?",
                                           (gametype.value,))
        if game_ids:
            game_ids = game_ids.fetchall()
            return [n[0] for n in game_ids]
        else:
            return None

    def get_played_games_for_player(self, player_id):
        game_ids = self.cursor.execute("SELECT game_id from player_game WHERE player_id = ?",
                                       (player_id,))
        if game_ids:
            game_ids = game_ids.fetchall()
            return [n[0] for n in game_ids]
        else:
            return None

    def get_played_games_of_type_for_player(self, gametype, player_id, season=[]):
        total_played_games = self.get_games_of_type(gametype, season)
        played_games_for_player = self.get_played_games_for_player(player_id)
        if played_games_for_player and total_played_games:
            return [n for n in played_games_for_player if n in total_played_games]
        else:
            return None

    def get_humiliation_games_for_player(self, player_id, gametype=[], season=[]):
        humiliation_games = self.get_humiliation_games(gametype, season)
        if gametype:
            cnt, last_places_for_player = self.stats_count_last_place_for_gametype(gametype, player_id)
        else:
            cnt, last_places_for_player = self.stats_count_last_place(player_id, season)
        if last_places_for_player and humiliation_games:
            return [n for n in last_places_for_player if n in humiliation_games]
        else:
            return None