from enum import Enum, unique

@unique
class GameType(Enum):
    THREE_ZERO_ONE = 0
    CRICKET = 1
    ALL_FIVES = 2
    AROUND_THE_CLOCK = 3

    @staticmethod
    def get_string(game_type):
        if game_type == GameType.THREE_ZERO_ONE:
            return '301'
        elif game_type == GameType.CRICKET:
            return 'Cricket'
        elif game_type == GameType.ALL_FIVES:
            return 'All Fives'
        elif game_type == GameType.AROUND_THE_CLOCK:
            return 'Ar. Clock'
        else:
            return str(game_type.value)

    @staticmethod
    def get_list_of_games():
        gametype_list = []
        for game in GameType:
            gametype_list.append(GameType.get_string(game))
        return gametype_list
