from PyQt4 import QtCore, QtGui, uic
from GameType import GameType

qtCreatorFile = "AddGameDialog.ui"  # Enter file here.
Ui_AddGameDialog, QtBaseClass = uic.loadUiType(qtCreatorFile)


class AddGameDialog(QtGui.QDialog, Ui_AddGameDialog):
    def __init__(self, parent=None):
        QtGui.QDialog .__init__(self, parent)
        self.setupUi(self)

        self.NumPlayers_spinBox.valueChanged.connect(self.update_players_list)
        self.players_listWidget.model().rowsMoved.connect(self.update_players_list)

    def update_players_list(self):
        for row in range(0, self.players_listWidget.count()):
            citem = self.players_listWidget.item(row)
            if row > self.NumPlayers_spinBox.value()-1:
                citem.setForeground(QtCore.Qt.red)
            else:
                citem.setForeground(QtCore.Qt.black)

    def get_current_ranking(self):
        ranking = []
        for row in range(0, self.NumPlayers_spinBox.value()):
            citem = self.players_listWidget.item(row)
            ranking.append(str(citem.text()))
        return ranking

    @staticmethod
    def get_game_values(players, current_season, max_season):
        print("get game values")
        ad = AddGameDialog()
        ad.season_spinBox.setMaximum(max_season)
        ad.season_spinBox.setValue(current_season)
        ad.NumPlayers_spinBox.setValue(len(players))
        ad.NumPlayers_spinBox.setMaximum(len(players))
        for game in GameType:
            ad.GameType_comboBox.addItem(GameType.get_string(game))
        for player in players:
            item = QtGui.QListWidgetItem()
            item.setText(str(player[2]))
            ad.players_listWidget.addItem(item)
        ret = ad.exec_()
        if ret:
            date = str(ad.calendarWidget.selectedDate().toString('yyyy:MM:dd'))
            gametype = GameType(ad.GameType_comboBox.currentIndex())
            season = ad.season_spinBox.value()
            rankings = ad.get_current_ranking()
            humiliation = ad.humiliation_checkBox.isChecked()
            return date, gametype, season, rankings, humiliation
        else:
            return None
